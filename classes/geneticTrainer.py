#! /usr/bin/python3
#
# geneticTrainer.py
#
# Provides a class that allows the training of an artificial neural net described through the class
# provided in ffnet.py.

import numpy as np
from classes import ffnet as ffn
import pickle

PROTECT = 20

class geneticTrainer:
    """A class that may generate a ffn.FeedForwardNet of which the fitness_function
    returns a value near zero.
    
    The fitness_function must accept a ffn.FeedForwardNet and may return a signed
    floating point number as a result to describe the 'goodness'. The returned number shall be non-positive!
    """

    def __init__(self, popSize, fitness, activity, layer_counts, protect, bias=[]):
        """Initializes member variables

        @param  popSize:      Maximum size of the population
        @param  fitness:      Function to evaluate fitness of a net (takes a net, gives an int, higher value is better)
        @param  activity:     Activity function for the neurons
        @param  layer_counts: List of counts of neurons for the hidden layers
        @param  bias:         Bias initialization for each net (default: 1 for each layer)
        """

        assert (popSize == abs(int(popSize))), "[ERR][GTR] popSize has to be a positive integer!"
        assert hasattr(fitness, '__call__'), "[ERR][GTR] fitness must be callable!"

        if bias == []:
            for x in range(len(layer_counts)-1):
                bias.append(1)

        self.mpopsize      = popSize
        self.mfitness      = fitness
        self.mactivity     = activity
        self.mlayer_counts = layer_counts
        self.mbias         = bias
        self.mprotect      = protect
        self.pop           = []
        self.gen           = 0
        self.oldfit        = 0
        self.veryold       = 0
        self.mutation_interval = 5

    def __sortpop(self):
        """Sorts the population. The fittest function will have the lowest index.
        """
        self.pop[:len(self.pop)-self.mprotect] = sorted(self.pop[:len(self.pop)-self.mprotect],
                                                        key=self.mfitness.autocall, reverse=True)

    def __decimate_pop(self):
        """Shrinks the population to the population size.

        As a side effect the population will be sorted after calling this function.
        The net with the highest fitness will be the one with the lowest index.
        """
        self.__sortpop()

        if self.mpopsize <= len(self.pop):
            del self.pop[self.mpopsize-self.mprotect:len(self.pop)-self.mprotect]

    def init_population(self, quality=1):
        """Initializes a sorted population.

        @param quality: If quality is 1, it will be a random population. There will be quality * population_size
                new nets of which the fittest will remain and be the new population.
        """
        #assert (not self.pop), "[ERR][GTR] Unable to initialize new population.\
# The population is not empty!"
        assert (quality >= 1), "[ERR][GTR] quality has to be a number larger or equal to one!"
        for x in range(self.mpopsize * quality):
            self.pop.append(self.__init_random_net())
        #just take the best of them all
        self.__decimate_pop()
        self.gen += 1

    def __init_random_net(self):
        """Initializes a new random net.

        @return: The net.
        """
        return ffn.FeedForwardNet(self.mactivity, self.mlayer_counts, self.mbias)

    def __step(self):
        """Takes the evolution one step further.
        """
        self.gen += 1
        newfit = self.mfitness(self.pop[0])
        if self.mutation_interval > 2:
            if abs(newfit) < 0.2:
                self.mutation_interval = 2
                print("Reached goal approximately.")
            else:
                if abs(newfit - self.oldfit) < 0.000000001 and abs(self.oldfit - self.veryold) < 0.000000001\
                    and self.mutation_interval != 50:
                    print("Maybe a local minimum is found!")
                    self.mutation_interval = 50
                else:
                    if newfit > 100:
                        self.mutation_interval = 100
                    else:
                        self.mutation_interval = 10

        for x in range(self.mpopsize -1):
            self.pop[x+1].mutate(self.pop[0], 0.3)
            self.pop.append(ffn.FeedForwardNet.getMutation(self.pop[x], 0.4/(0.3*np.log(self.gen)+1),
                                                           self.mutation_interval))

        self.pop.append(self.__init_random_net())

        print(newfit)
        self.veryold = self.oldfit
        self.oldfit = newfit

        self.__decimate_pop()

    def train(self, generations):
        """Trains the net for a fixed count of generations.

        @param generations: Count of generations.
        """
        for x in range(generations):
            self.__step()

    def save(self, file_path):
        """Saves the population and other training data in binary file
        @param file_path: path to the file the net should be written to
        @return: None
        """
        pickle.dump(self, open(file_path, "wb"))

    @classmethod
    def load(cls, file_name):
        """Loads a training set.

        @param file_name: path to the file
        @return: loaded object of type geneticTrainer
        """
        return pickle.load(open(file_name, "rb"))