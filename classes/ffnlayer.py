#! /usr/bin/python3
#
# ffnlayer.py
#
# Implementation of a symmetric layer of a feed forward neural network

import numpy as np

INTERVALSIZE = 15

class FfnLayer:
    """
    :Implementation of a symmetric layer of a feed forward neural network
    """
    def __init__(self, neuron_count, input_count, activity_function, bias=0,
                 createWeightMatrix = True):
        """
        : set weight matrix and store values to self
        """

        self.bias = bias
        self.input_count = int(input_count)+1  # number of inputs plus bias
        self.neuron_count = int(neuron_count)  # number of layer neurons
        self.activity_function = activity_function  # maps activity to output
        self.delta_factor_vector = None  # used later for backpropagation
        if createWeightMatrix:
            self.weight_matrix = np.matrix(
                np.random.random([self.neuron_count,
                                  self.input_count]) * INTERVALSIZE - (int (INTERVALSIZE/2)))

    # Mutation constructor
    @staticmethod
    def getMutation(origin, probability, mutation_interval):
        copy = FfnLayer(origin.neuron_count, origin.input_count-1,
                        origin.activity_function, origin.bias, False)

        copy.weight_matrix = origin.weight_matrix.copy()

        for x in np.nditer(copy.weight_matrix, op_flags=['readwrite']):
            if np.random.random_sample() < probability:
                x[...] += np.random.random_sample()*mutation_interval - (int (mutation_interval/2))

        return copy

    def mutate(self, bestlayer, probability):
        for i in range(self.weight_matrix.shape[0]):
            for j in range(self.weight_matrix.shape[1]):
                if np.random.random_sample() < probability:
                    self.weight_matrix.itemset((i,j), bestlayer.weight_matrix.item(i,j))
                    return
                #if np.random.random_sample() < 0:
                #    self.weight_matrix.itemset((i,j), np.random.random_sample()*2 -1)

    # Copy constructor
    @staticmethod
    def getCopy(origin):
        copy = FfnLayer(origin.neuron_count, origin.input_count,
                        origin.activity_function, origin.bias, False)
        copy.weight_matrix = origin.weight_matrix.copy()
        return copy

    def output_vector(self, input_vector):
        """
        : Calculates activity level and returns layer output
        :
        : returns: layer output as np.matrix
        """
        # add bias value to input vector
        input_vector = np.insert(
            input_vector, 0, self.bias, axis=0)  # target, pos, value, axis

        # check input vector to have 1 column
        assert (input_vector.shape[1] == 1), \
            "[ERR][NLY] input_vector has {} columns, expected 1!".format(
                input_vector.shape[1])

        # check input_vector rows to match number of inputs
        assert (input_vector.shape[0] == self.input_count), \
            "[ERR][NLY] input_vector has {} entries, expected {}!".format(
                input_vector.shape[0], self.input_count)

        # calculate activity level and apply activity function
        activity_level = self.weight_matrix * input_vector
        output_vector = self.activity_function.function(activity_level)

        return output_vector

