__author__ = 'Lasse Schuirmann'

from classes import ffnet as ffn
import threading
import queue


class FitnessThread(threading.Thread):
    def __init__(self, net, qsample, qresult):
        threading.Thread.__init__(self)
        self.qsample = qsample
        self.qresult = qresult

        self.fit = 0
        self.net = net

    def run(self):
        #print("Run started. Getting first sample...")
        next = self.qsample.get()
        while next != "finish":
            self.fit += sum(abs(self.net.output_vector(next[0]) - next[1]))
            #print("Getting next sample...")
            next = self.qsample.get()

        #print("Putting result.")
        self.qresult.put(self.fit)



class Fitness:
    """Provides a generic fitness class.

    Just give your input samples and desired output samples into the constructor.
    """

    def __init__(self, data_set):
        """Constructor for the generic fitness function.

        @param data_set: is a list of n sublists containing an input vector followed by the output vector.
        """
        # TODO check if data set is in the correct form
        self.data_set = data_set


    def __threaded(self, net, threads):
        assert (threads > 0), "[ERR][FIT] The count of threads has to be positive!"

        fit = 0
        pool = []
        qsample = queue.Queue(threads * 2)
        qresult = queue.Queue()

        #print("Starting threads.")

        for i in range(threads):
            thread = FitnessThread(net, qsample, qresult)
            pool.append(thread)
            thread.start()

        #print("Putting items into sample queue.")

        for item in self.data_set:
            qsample.put(item)

        for i in range(threads):
            qsample.put("finish")

        #print("Waiting for sample queue to be emptied.")

        #print("Waiting for threads to join.")

        for thread in pool:
            thread.join()

        #print("Calculate result.")

        while not qresult.empty():
            fit -= qresult.get()

        return fit

    def __single_threaded(self, net, printit):
        fit = 0

        if printit:
            print("_" * 40)

        for ninput, output in self.data_set:
            netoutput = net.output_vector(ninput)
            fit -= sum(abs(output - netoutput))
            if printit:
                print("Exp: ", output)
                print("Got: ", netoutput)

        if printit:
            print("=" * 40)
            print("Fitness: ", fit)
            print("=" * 40)

        return fit

    def autocall(self, net):
        return self.__call__(net)

    def __call__(self, net, printit=False, multithreaded = 0):
        """Gets the fitness.

        @param net: The net to be tested.
        @param printit: If this is set to True it will print advanced information about the fitness.
        @param multithreaded: If this is set to a value larger than zero multiple threads will be created to determine
            the fitness faster if there are many samples. The value is the amount of threads that will be created.
        @return: The fitness.
        """

        assert isinstance(net, ffn.FeedForwardNet), "[ERR][FIT] The parameter is no FeedForwardNet!"

        if multithreaded == 0:
            return self.__single_threaded(net, printit)
        else:
            if printit:
                print("Please use single-threaded fitness function to access the print feature!")
            return self.__threaded(net, multithreaded)
