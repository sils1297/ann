#! /usr/share/python3

import math
import numpy as np


class Linear:

    def __init__(self, slope=1, intercept=0):
        self.slope = slope
        self.intercept = intercept

    def function(self, x):
        return self.slope*x+self.intercept

    def derivative(self, x_vector):
        return np.matrix([self.slope]).repeat(x_vector.shape[0], 0)


class LinearThreshold:

    def __init__(self, slope=1, intercept=0, threshold=0):
        self.slope = slope
        self.intercept = intercept
        self.threshold = threshold

    def function(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):
            if x_vector.item(i) >= self.threshold:
                x_vector.itemset(i, self.slope*x_vector.item(i)+self.intercept)
            else:
                x_vector.itemset(i, self.intercept)
        return x_vector

    def derivative(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):
            if x_vector.item(i) >= self.threshold:
                x_vector.itemset(i, self.slope)
            else:
                x_vector.itemset(i, 0)
        return x_vector


class Binary:

    def __init__(self, left=0, right=1, threshold=0):
        self.left = left
        self.right = right
        self.threshold = threshold

    def function(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):  # iterate through vector entries
            if x_vector.item(i) >= self.threshold:  # if above threshold
                x_vector.itemset(i, self.right)  # replace with right value
            else:
                x_vector.itemset(i, self.left)
        return x_vector

    def derivative(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        return np.matrix([0]).repeat(x_vector.shape[0], 0)


class Logistic:


    def __init__(self, gain=1):
        self.gain = gain

    def function(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):
            x_vector.itemset(i, 1/(1+math.exp(-1*self.gain*x_vector.item(i))))
        return x_vector

    def derivative(self, vector):

        x_vector = np.matrix(vector)  # copy because vector is mutable!
        function_value = self.function(x_vector)
        for i in range(x_vector.shape[0]):
            x_vector.itemset(i,
                             function_value.item(i)*(1-function_value.item(i)))
        return x_vector


class HyperbolicTangent:

    def __init__(self):
        pass

    def function(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):
            x_vector.itemset(i, math.tanh(x_vector.item(i)))
        return x_vector

    def derivative(self, vector):
        x_vector = np.matrix(vector)  # copy because vector is mutable!
        for i in range(x_vector.shape[0]):
            x_vector.itemset(i, 1 - math.tanh(x_vector.item(i))**2)
        return x_vector