#! /usr/bin/python3
#
# ffnet.py
#
#

from classes import ffnlayer
import numpy as np
import pickle


class FeedForwardNet:
    """
    : Class representing a standard symmetric feed forward neural net
    """

    def __init__(self, activity_function, layer_neuron_counts, biases=[]):
        """
        : initialize neural network
        """
        if not biases: biases = []
        if len(biases) < len(layer_neuron_counts) - 1:
            biases = [0 for lnc in layer_neuron_counts][:-1]

        self.layers = []  # Python structure used here!

        for i in range(len(
                layer_neuron_counts) - 1):  # no need for input layer
            new_layer = ffnlayer.FfnLayer(layer_neuron_counts[i + 1], # neurons
                                          layer_neuron_counts[i], # input
                                          activity_function,
                                          biases[i])
            self.layers.append(new_layer)
        self.activity_function = activity_function
        self.gen = 0
    
    # This constructor initiates a mutation
    @staticmethod
    def getMutation(origin, probability, mutation_interval):
        """Creates a new net derived from origin.

        @param origin:
        @param probability:
        @param mutation_interval:
        @return:
        """
        copy = FeedForwardNet(origin.activity_function, [1])
        copy.layers = []
        for layer in origin.layers:
            copy.layers.append(ffnlayer.FfnLayer.getMutation(layer, probability, mutation_interval))
        return copy

    def mutate(self, bestnet, probability):
        for index, layer in enumerate(self.layers):
            layer.mutate(bestnet.layers[index], probability)
        self.gen += 1

    # This constructor initiates a copy
    @staticmethod
    def getCopy(origin):
        #Create empty net
        copy = FeedForwardNet(origin.activity_function, 1)
        copy.layers = []
        for layer in origin.layers:
            copy.layers.append(ffnlayer.FfnLayer.getCopy(layer))
        return copy

    @classmethod
    def load(cls, file_name):
        """

        @param file_name: path to the file the net should ne loaded from
        @return: loaded Neural Net as ffnet.FeedForwardNet Class
        """
        return pickle.load(open(file_name, "rb"))

    def save(self, file_path):
        """
        saves neural net in binary file
        @param file_path: path to the file the net should be written to
        @return: None
        """
        pickle.dump(self, open(file_path, "wb"))

    def output_vector(self, input_vector):
        """
        : passes the inputs given with input_vector through the whole net
        :
        : returns: output as np.matrix
        """

        for layer in self.layers:
            input_vector = layer.output_vector(input_vector)

        return input_vector

    def train_bp(self, data_set, max_runs=10000,
                 min_quality=.95, epsilon_factor=1):
        """
        trains network on a given data set using backpropagation

        @param data_set: desired in- and outputs as either a list of pairs
                         of np.matrix elements or a file with lines as follows:
                         IN1 IN2 IN3 IN4: OUT1 OUT2 OUT3
        @param max_runs: training will be aborted after this number of runs,
                         even if the desired quality is not yet reached
        @param min_quality: training will be aborted if the net returns the
                            desired output with this probability in average.

        @return: success percentage on training set
        """

        # prepare data set (TODO: convert to dict if file)
        #assert (type(data_set) == dict), \
        #   "[ERR][TBP] data_set has to be dict for now :("

        # TODO: while loop
        for input_vector, desired_output_vector in data_set:

            # forward pass:
            net_input_vectors = [input_vector]  # weight_matrix*input
            input_vectors = [input_vector]  # net_input after applying acf
            for i in range(len(self.layers)):  # pass forward and store

                # add layer bias to input
                input_vectors[i] = np.insert(input_vectors[i], 0,
                                             self.layers[i].bias, axis=0)

                # calculate net_input and append to net_input_vectors:
                net_input_vectors.append(
                    self.layers[i].weight_matrix * input_vectors[i])

                # apply activity function and append to input_vectors:
                input_vectors.append(
                    self.layers[i].activity_function.function(
                        net_input_vectors[i + 1]))

            # error determination and backward pass (starting from behind)
            for i in range(len(self.layers) - 1, -1, -1):

                if i == len(self.layers) - 1:  # output-layer

                    # delta factor = f'(net_input)*(output_desired-output_real)
                    delta_factor_vector = \
                        self.layers[i].activity_function.derivative(
                            net_input_vectors[i+1])  # input contains bias
                    output_diff_vector = (
                        desired_output_vector - input_vectors[i+1])  # same
                    for ii in range(delta_factor_vector.shape[0]):
                        delta_factor_vector.itemset(ii,
                            delta_factor_vector.item(ii) *
                                output_diff_vector.item(ii))

                    # store delta_factor_vector in layer
                    self.layers[i].delta_factor_vector = delta_factor_vector

                else:  # not output layer

                    # delta factor =
                    # f'(net_input) * sum(unit_next_layer*weight_here_there)
                    delta_factor_vector = \
                        self.layers[i].activity_function.derivative(
                            net_input_vectors[i + 1])  # input contains bias
                    delta_connection_sum_vector = np.matrix([0.]).repeat(
                        delta_factor_vector.shape[0], 0)
                    for ii in range(delta_connection_sum_vector.shape[0]):
                        dcsum = 0
                        for N in range(self.layers[i+1].weight_matrix.shape[0]):
                            dcsum += self.layers[i+1].weight_matrix[N, ii+1] *\
                                self.layers[i+1].delta_factor_vector[N]
                        delta_connection_sum_vector.itemset(ii, dcsum)
                    for ii in range(delta_factor_vector.shape[0]):
                        delta_factor_vector.itemset(ii,
                            delta_factor_vector.item(ii) *
                                delta_connection_sum_vector.item(ii))
                        # CAN REACH INF if DFV explodes <- why ever?
                        # weight_matrix layer 0:
                        #   [[  0.13795915 -15.78384901  -2.70155476]
                        #    [  0.36742152   1.73206761  15.22383452]]
                        # weight_matrix layer 1:
                        #   [[-0.97028744 -0.91152043 -7.17410428]]
                        #
                        # input: [[1],[1]]
                        # desired_output: [[0]]
                        # dfv1: 13.1 ?
                        # dfv0: -799 ?
                        #
                        # input: [[1],[0]]
                        # desired_output: [[1]]
                        # dfv1: -21279.57576936?
                        # dfv0: -9.22299332e+10
                        #
                        # input: [[1],[0]]
                        # desired_output: [[1]]
                        # dfv1: -21279.57576936?
                        # dfv0: -9.22299332e+10
                        #
                        # soon:
                        #input_vector: [[1]
                        #               [0]]
                        #desired_output_vector: [[1]]
                        #dfv1: [[1.16380631e+135]]
                        #./ classes / ffnet.py:139: RuntimeWarning: overflow
                        #encountered in multiply
                        #delta_factor_vector.itemset(ii, dfv0[[-inf]
                        #                                     [inf]]

                    # store delta_factor_vector in layer
                    self.layers[i].delta_factor_vector = delta_factor_vector

                # changing layer weights:

                #delta_weight(to->from)
                #= epsilon_factor*delta_factor*output(from)
                #it will be applied directly on the weight matrix
                #(weight = weight + delta_weight)
                #weight_matrix:  Bias  Input1 Input2 ...
                #        Neuron0  X      X      X
                #        Neuron1  X      X      X

                # iterate through neurons:
                for N in range(self.layers[i].weight_matrix.shape[0]):
                    # iterate through inputs:
                    for I in range(self.layers[i].weight_matrix.shape[1]):

                        if I == 0:  # bias
                            self.layers[i].weight_matrix[N, I] += \
                                epsilon_factor * \
                                delta_factor_vector[N] * \
                                self.layers[i].bias
                        else:  # further inputs
                            self.layers[i].weight_matrix[N, I] += \
                                epsilon_factor * \
                                delta_factor_vector[N] * \
                                input_vectors[i][I]