#! /usr/bin/python3
#
# ann.py
#
# Test for the feedForwardNet
import numpy as np
from classes import ffnet as ffn
from classes import geneticTrainer as gen
from classes import activity_functions as acf
from classes import fitness


def test_genetic_trainer():

    fit = fitness.Fitness(get_bin_data_set())

    if input("Do you want to load training data from file? [y/n]") == "y":
        trainer = gen.geneticTrainer.load(input("Please enter a file name: "))
    else:
        print("Initializing new training set. For more options please edit the code of this file.")
        activity = acf.Logistic()
        trainer = gen.geneticTrainer(50, fit, activity, [7, 8, 3], 20)
        trainer.init_population(2)

    trainer.train(int(input("How many generations do you want? ")))
    fit(trainer.pop[0], True, 0)
    while input("Do you want to train more? [y/n]") == "y":
        trainer.train(int(input("How many generations do you want? ")))
        fit(trainer.pop[0], True, 0)

    if input("Do you want to save the net? [y/n]") == "y":
        trainer.pop[0].save(input("Please enter a file name: "))

    if input("Do you want to save the training set? [y/n]") == "y":
        trainer.save(input("Please enter a file name: "))

    fit(trainer.pop[0], True, 0)



    print("Matrices:")
    print(trainer.pop[0].layers[0].weight_matrix)
    print(trainer.pop[0].layers[1].weight_matrix)


def get_xor_data_set():
    input1 = np.matrix([[1], [1]])
    input2 = np.matrix([[1], [0]])
    input3 = np.matrix([[0], [1]])
    input4 = np.matrix([[0], [0]])
    output1 = np.matrix([[0]])
    output2 = np.matrix([[1]])
    output3 = np.matrix([[1]])
    output4 = np.matrix([[0]])
    data_set = [[input1, output1],
                [input2, output2],
                [input3, output3],
                [input4, output4]]
    return data_set


def get_bin_data_set():
        input0 = np.matrix([[0],[0],[0],[0],[0],[0],[0]])
        input1 = np.matrix([[0],[0],[0],[0],[0],[0],[1]])
        input2 = np.matrix([[0],[0],[0],[0],[0],[1],[0]])
        input3 = np.matrix([[0],[0],[0],[0],[1],[0],[0]])
        input4 = np.matrix([[0],[0],[0],[1],[0],[0],[0]])
        input5 = np.matrix([[0],[0],[1],[0],[0],[0],[0]])
        input6 = np.matrix([[0],[1],[0],[0],[0],[0],[0]])
        input7 = np.matrix([[1],[0],[0],[0],[0],[0],[0]])

        output0 = np.matrix([[0],[0],[0]])
        output1 = np.matrix([[0],[0],[1]])
        output2 = np.matrix([[0],[1],[0]])
        output3 = np.matrix([[0],[1],[1]])
        output4 = np.matrix([[1],[0],[0]])
        output5 = np.matrix([[1],[0],[1]])
        output6 = np.matrix([[1],[1],[0]])
        output7 = np.matrix([[1],[1],[1]])

        data_set = [[input0, output0],
                    [input1, output1],
                    [input2, output2],
                    [input3, output3],
                    [input4, output4],
                    [input5, output5],
                    [input6, output6],
                    [input7, output7]]

        return data_set


def verbose_ffnet_test():
    # defining network
    layers = []
    biases = []

    input_finished = False

    layers.append(int(input("Please enter number of network inputs:")))
    while not input_finished:
        try:
            count = (int(input(
                "Please enter neuron count of next layer (0 to finish):")))
            if count == 0:
                input_finished = True
            else:
                layers.append(count)
                biases.append(float(input("Please enter bias of this layer:")))
        except TypeError:
            print("ignored bad input, please retry!")

    # defining input vector
    input_vector = np.matrix(
        [float(input("Please enter 1. value of input vector!"))])
    for i in range(layers[0] - 1):
        input_vector = np.append(input_vector, [[float(
            input("Please enter {}. value of input vector!".format(i + 2)))]],
                                 axis=0)

    # create layer and calculate output
    ffnet = ffn.FeedForwardNet(acf.Binary(), layers, biases)
    output_vector = ffnet.output_vector(input_vector)

    # print stuff:
    print("*" * 80)
    print("Neural net weight matrices:")
    for layer in ffnet.layers:
        print(layer.weight_matrix)
    print("Layer Biases:\n", biases)
    print("Input Vector:\n", input_vector)
    print("Output Vector:\n", output_vector)
    print("*" * 80)


def quick_bp_xor_test():
    acf_log = acf.Logistic()
    input1 = np.matrix([[1], [1]])
    input2 = np.matrix([[1], [0]])
    input3 = np.matrix([[0], [1]])
    input4 = np.matrix([[0], [0]])
    output1 = np.matrix([[0]])
    output2 = np.matrix([[1]])
    output3 = np.matrix([[1]])
    output4 = np.matrix([[0]])
    data_set = [[input1, output1],
                [input2, output2],
                [input3, output3],
                [input4, output4]]
    xornet = ffn.FeedForwardNet(acf_log, [2, 3, 1], [0, 0])

    xornet.train_bp(data_set, 100000, .0001, .66)
    print("*"*40)
    print("[1][1] (0) ->", xornet.output_vector(input1))
    print("[1][0] (1) ->", xornet.output_vector(input2))
    print("[0][1] (1) ->", xornet.output_vector(input3))
    print("[0][0] (0) ->", xornet.output_vector(input4))
    print(xornet.layers[0].weight_matrix)
    print(xornet.layers[1].weight_matrix)
    fitness = 0
    fitness -= abs(xornet.output_vector([[0], [0]]))
    fitness -= abs(xornet.output_vector([[0], [0]]))
    fitness -= abs(xornet.output_vector([[0], [0]]))
    fitness -= abs(xornet.output_vector([[0], [0]]))
    print("Fitness:", fitness)
    print("*"*40)


def binary_bp_test():

    acf_log = acf.Logistic()
    binary_net = ffn.FeedForwardNet(acf_log, [7, 8, 3], [0, 0])

    input0 = np.matrix([[0], [0], [0], [0], [0], [0], [0]])
    input1 = np.matrix([[0], [0], [0], [0], [0], [0], [1]])
    input2 = np.matrix([[0], [0], [0], [0], [0], [1], [0]])
    input3 = np.matrix([[0], [0], [0], [0], [1], [0], [0]])
    input4 = np.matrix([[0], [0], [0], [1], [0], [0], [0]])
    input5 = np.matrix([[0], [0], [1], [0], [0], [0], [0]])
    input6 = np.matrix([[0], [1], [0], [0], [0], [0], [0]])
    input7 = np.matrix([[1], [0], [0], [0], [0], [0], [0]])

    output0 = np.matrix([[0], [0], [0]])
    output1 = np.matrix([[0], [0], [1]])
    output2 = np.matrix([[0], [1], [0]])
    output3 = np.matrix([[0], [1], [1]])
    output4 = np.matrix([[1], [0], [0]])
    output5 = np.matrix([[1], [0], [1]])
    output6 = np.matrix([[1], [1], [0]])
    output7 = np.matrix([[1], [1], [1]])

    data_set = [[input0, output0],
                [input1, output1],
                [input2, output2],
                [input3, output3],
                [input4, output4],
                [input5, output5],
                [input6, output6],
                [input7, output7]]


    binary_net.train_bp(data_set, 100000, .0001, .66)
    print("*" * 40)
    print("[0,0,0,0,0,0,0]: (0,0,0) ->", np.transpose(binary_net.output_vector(input0)))
    print("[0,0,0,0,0,0,1]: (0,0,1) ->", np.transpose(binary_net.output_vector(input1)))
    print("[0,0,0,0,0,1,0]: (0,1,0) ->", np.transpose(binary_net.output_vector(input2)))
    print("[0,0,0,0,1,0,0]: (0,1,1) ->", np.transpose(binary_net.output_vector(input3)))
    print("[0,0,0,1,0,0,0]: (1,0,0) ->", np.transpose(binary_net.output_vector(input4)))
    print("[0,0,1,0,0,0,0]: (1,0,1) ->", np.transpose(binary_net.output_vector(input5)))
    print("[0,1,0,0,0,0,0]: (1,1,0) ->", np.transpose(binary_net.output_vector(input6)))
    print("[1,0,0,0,0,0,0]: (1,1,1) ->", np.transpose(binary_net.output_vector(input7)))

    print(binary_net.layers[0].weight_matrix)
    print(binary_net.layers[1].weight_matrix)
    fitness = 0
    for input_vector, desired_output_vector in data_set:
        output_vector = binary_net.output_vector(input_vector)
        for i in range(output_vector.shape[0]):
            fitness -= abs(desired_output_vector.item(i)
                           - output_vector.item(i))
    print(fitness)
    print("*" * 40)


if __name__ == "__main__":

#    quick_bp_xor_test()
#    binary_bp_test()
    test_genetic_trainer()
#    test_genetic_trainer()
