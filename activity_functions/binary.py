#! /usr/share/python


class Binary:

    def __init__(self, left=0, right=1, threshold=0):
        self.left = left
        self.right = right
        self.threshold = threshold

    def function(self, x):
        if x >= self.threshold:
            return self.right
        else:
            return self.left

    def derivative(self, x):
        return 0
