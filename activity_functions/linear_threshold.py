#! /usr/share/python


class LinearThreshold:

    def __init__(self, slope=1, intercept=0, threshold=0):
        self.slope = slope
        self.intercept = intercept
        self.threshold = threshold

    def function(self, x):
        if x >= self.threshold:
            return self.slope*x+self.intercept
        else:
            return self.intercept

    def derivative(self, x):
        if x >= self.threshold:
            return self.slope
        else:
            return 0