#! /usr/share/python


class Linear:

    def __init__(self, slope=1, intercept=0):
        self.slope = slope
        self.intercept = intercept

    def function(self, x):
        return self.slope*x+self.intercept

    def derivative(self, x):
        return self.slope
