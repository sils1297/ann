#! /usr/share/python
import cmath


class HyperbolicTangent:

    def __init__(self):
        pass

    def function(self, x):
        return cmath.tanh(x).real

    def derivative(self, x):
        return (1 - cmath.tanh(x)**2).real