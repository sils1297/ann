#! /usr/share/python

import cmath


class Logistic:

    def __init__(self, gain=1):
        self.gain = gain

    def function(self, x):
        return 1/(1+cmath.exp(-1*self.gain*x)).real

    def derivative(self, x):
        function_value = self.function(x)
        return function_value - (1 - function_value).real